import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.Random;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static java.lang.Math.ceil;

public class TrainAgent extends Agent {
    public String _name;

    public long _typeID;
    public long _busyTime;
    public long _velocity;

    public LinkedHashMap<String, Map<String, Double>> _cargos;
    public LinkedHashMap<String, AID> _senders;

    @Override
    protected void setup() {
        Object args[] = getArguments();

        _name = (String) args[0];
        _typeID = (long) args[1];
        _velocity = (long) args[2];
        _busyTime = 0;
        _cargos = new LinkedHashMap<>();
        _senders = new LinkedHashMap<>();

        System.out.println(getLocalName() + "(" + _typeID + ") is ready!");

        registerService();

        addBehaviour(new waitCargo());
    }

    private void registerService() {
        DFAgentDescription dfa = new DFAgentDescription();
        dfa.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("Train");
        sd.setName(getLocalName());
        dfa.addServices(sd);

        try {
            DFService.register(this, dfa);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public class waitCargo extends CyclicBehaviour {
        @Override
        public void action() {
            ACLMessage msg = myAgent.receive();
            if (msg != null) {
                switch (msg.getPerformative()) {
                    case ACLMessage.INFORM:
                        ACLMessage messageToCargo = new ACLMessage(ACLMessage.PROPOSE);

                        var parser1 = new JSONParser();
                        JSONObject jsonObject1 = null;

                        try {
                            jsonObject1 = (JSONObject) parser1.parse(msg.getContent());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        long distance = (long) jsonObject1.get("distance");

                        long duration = (long) ceil((double) distance / (double) _velocity);

                        String cargoName = (String) jsonObject1.get("cargoName");

                        JSONObject jsonToCargo = new JSONObject();
                        jsonToCargo.put("name", getLocalName());
                        jsonToCargo.put("duration", duration);
                        jsonToCargo.put("busy", _busyTime);
                        messageToCargo.setContent(jsonToCargo.toJSONString());
                        messageToCargo.addReceiver(_senders.get(cargoName));

                        myAgent.send(messageToCargo);
                        break;
                    case ACLMessage.REQUEST:
                        var parser = new JSONParser();
                        JSONObject jsonObject = null;

                        try {
                            jsonObject = (JSONObject) parser.parse(msg.getContent());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        long typeID = (long) jsonObject.get("typeID");
                        long from = (long) jsonObject.get("from");
                        long to = (long) jsonObject.get("to");

                        if (typeID == _typeID) {
                            // request to server

                            _senders.put(msg.getSender().getLocalName(), msg.getSender());

                            ACLMessage messageToServer = new ACLMessage(ACLMessage.REQUEST);
                            DFAgentDescription tmpServer = new DFAgentDescription();
                            ServiceDescription sdServer = new ServiceDescription();
                            sdServer.setType("ServerAgent");
                            tmpServer.addServices(sdServer);

                            try {
                                DFAgentDescription[] res = null;

                                while (res == null || res != null && res.length < 1) {
                                    res = DFService.search(myAgent, tmpServer);
                                }

                                JSONObject jsonToServer = new JSONObject();
                                jsonToServer.put("cargoName", msg.getSender().getLocalName());
                                jsonToServer.put("from", from);
                                jsonToServer.put("to", to);

                                messageToServer.setContent(jsonToServer.toJSONString());
                                messageToServer.addReceiver(res[0].getName());

                                myAgent.send(messageToServer);

                                System.out.println(" = " + getLocalName() + " requested distance for (" +
                                        msg.getSender().getLocalName() + ")");
                            } catch (FIPAException e) {
                                e.printStackTrace();
                            }
                        }

                        break;

                    case ACLMessage.AGREE:
                        var parser2 = new JSONParser();
                        JSONObject jsonFromCargo = null;

                        try {
                            jsonFromCargo = (JSONObject) parser2.parse(msg.getContent());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        cargoName = msg.getSender().getLocalName();
                        long busy = (long) jsonFromCargo.get("busy");
                        duration = (long) jsonFromCargo.get("duration");

                        ACLMessage responseToCargo;

                        if (_busyTime > busy) {
                            // this train is already busy
                            responseToCargo = new ACLMessage(ACLMessage.REFUSE);
                            responseToCargo.addReceiver(msg.getSender());

                            myAgent.send(responseToCargo);

                            System.out.println(" - REFUSED: (" + getLocalName() + " | " + cargoName + ")");
                        }
                        else {
                            responseToCargo = new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);
                            responseToCargo.addReceiver(msg.getSender());

                            myAgent.send(responseToCargo);

                            System.out.println(" + ACCEPTED: (" + getLocalName() + " | " + cargoName + ")");

                            Map<String, Double> data = new HashMap<>();
                            data.put("duration", (double) duration);

                            _cargos.put(cargoName, data);

                            _busyTime += duration;
                        }

                        break;

                    case ACLMessage.PROPAGATE:
                        JSONObject jsonToMain = convertData();

                        jsonToMain.put("name", getLocalName());

                        ACLMessage messageToMain = new ACLMessage(ACLMessage.CANCEL);
                        messageToMain.setContent(jsonToMain.toJSONString());
                        messageToMain.addReceiver(msg.getSender());

                        myAgent.send(messageToMain);

                        break;
                }
            }
        }

        private JSONObject convertData() {
            JSONObject output = new JSONObject();

            ArrayList<String> scheduleList = new ArrayList<>();

            for (var name: _cargos.keySet()) {
                int N = _cargos.get(name).get("duration").intValue();

                for (int i = 0; i < N; i++) {
                    scheduleList.add(name);
                }
            }

            output.put("schedule", scheduleList);

            return output;
        }
    }

    @Override
    protected void takeDown() {
        // console log Train + all his cargos
        System.out.println(_name + "(" + _typeID + ") = {");
        for (var name: _cargos.keySet()) {
            System.out.println("\t" + name + ": \t" + _cargos.get(name).get("duration").intValue());
        }
        System.out.println("} (time: " + _busyTime + ")\n");

        System.out.println(getAID().getLocalName() + " destroyed\n");
    }
}