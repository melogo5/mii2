import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.wrapper.AgentController;
import jade.wrapper.ControllerException;
import jade.wrapper.StaleProxyException;

import java.io.*;
import java.util.ArrayList;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class MainAgent extends Agent {
    long _cargosAmount;
    long _cargosCounter;

    long _trainsAmount;
    long _trainsCounter;

    Map<String, ArrayList<String>> _schedule;

    @Override
    protected void setup() {
        Object args[] = getArguments();

        _cargosCounter = 0;
        _trainsCounter = 0;

        _schedule = new HashMap<>();

        registerService();

        addBehaviour(new waitFinish());

        String filePath = (String) args[0];
        // parsing & create agents
        var parser = new JSONParser();
        try (Reader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), StandardCharsets.UTF_8))) {
            var jsonInput = (JSONObject) parser.parse(reader);

            // trains
            var trains = (JSONArray) jsonInput.get("trains");
            _trainsAmount = trains.size();
            Map<Integer, Integer> amount = new HashMap<>();

            for (Object o : trains) {
                if (o instanceof JSONObject train) {
                    String name = (String) train.get("name");
                    long typeID = (long) train.get("typeID");
                    long velocity = (long) train.get("velocity");

                    int id = (int) typeID;

                    if (amount.containsKey(id)) {
                        int k = amount.get(id);
                        amount.put(id, k + 1);
                    } else {
                        amount.put(id, 1);
                    }

                    Object[] trainModel = {
                            name, typeID, velocity
                    };

                    createAgent(trainModel,
                            "Train(" + name + ")",
                            "TrainAgent"
                    );
                }
            }

            // cargos
            var cargos = (JSONArray) jsonInput.get("cargos");
            _cargosAmount = cargos.size();

            long cargoID = 0;
            for (Object o : cargos) {
                if (o instanceof JSONObject cargo) {
                    long typeID = (long) cargo.get("typeID");
                    long from = (long) cargo.get("from");
                    long to = (long) cargo.get("to");
                    long countTrains = amount.get((int) typeID);

                    Object[] cargoModel = {
                            typeID, from, to, countTrains, getAID()
                    };

                    createAgent(cargoModel,
                            "Cargo" + Long.toString(cargoID),
                            "CargoAgent"
                    );

                    cargoID++;
                }
            }
        } catch (IOException e) {
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void registerService() {
        DFAgentDescription dfa = new DFAgentDescription();
        dfa.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("MainAgent");
        sd.setName(getLocalName());
        dfa.addServices(sd);

        try {
            DFService.register(this, dfa);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }

        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void createAgent(Object[] agentModel, String name, String agentName) {
        AgentController ac = null;

        try {
            ac = getContainerController().createNewAgent(name, agentName, agentModel);
        } catch (StaleProxyException e) {
            System.out.println("error1!!");
            e.printStackTrace();
        }

        if (ac != null) {
            try {
                ac.start();
            } catch (StaleProxyException e) {
                System.out.println("error2!!");
                e.printStackTrace();
            }
        }
    }

    public class waitFinish extends CyclicBehaviour {
        @Override
        public void action() {
            ACLMessage msg = myAgent.receive();
            if (msg != null) {
                switch (msg.getPerformative()) {
                    // message from Cargo
                    case ACLMessage.INFORM:
                        _cargosCounter++;
                        System.out.println("\nMAIN: " + _cargosCounter + " / " + _cargosAmount
                                + " (" + msg.getSender().getLocalName() + ")\n");

                        if (_cargosCounter == _cargosAmount) {
                            System.out.println("\n\n*** Проверили все грузы ***\n\n");

                            ACLMessage messageToTrains = new ACLMessage(ACLMessage.PROPAGATE);
                            DFAgentDescription tmpTrains = new DFAgentDescription();
                            ServiceDescription sdTrains = new ServiceDescription();
                            sdTrains.setType("Train");
                            tmpTrains.addServices(sdTrains);

                            try {
                                DFAgentDescription[] res = null;

                                while (res == null || res != null && res.length < 1) {
                                    res = DFService.search(myAgent, tmpTrains);
                                }

                                for (var train: res) {
                                    messageToTrains.addReceiver(train.getName());
                                }

                                myAgent.send(messageToTrains);
                            } catch (FIPAException e) {
                                e.printStackTrace();
                            }
                        }
                        break;

                    // message from Train
                    case ACLMessage.CANCEL:
                        _trainsCounter++;

                        var parser = new JSONParser();
                        JSONObject jsonFromTrain = null;

                        try {
                            jsonFromTrain = (JSONObject) parser.parse(msg.getContent());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        String trainName = (String) jsonFromTrain.get("name");
                        ArrayList<String> schedule = (ArrayList<String>) jsonFromTrain.get("schedule");

                        System.out.println("\nMAIN: " + _trainsCounter + " / " + _trainsAmount
                                + " (" + trainName + " loading own schedule)\n");

                        _schedule.put(trainName, schedule);

                        if (_trainsCounter == _trainsAmount) {
                            System.out.println("\n\n*** PROGRAM SHUTDOWN ***\n\n");

                            try {
                                getContainerController().kill();
                            } catch (ControllerException e) {
                                e.printStackTrace();
                            }
                        }

                        break;
                }
            }
        }
    }

    private void outputSchedule(String filepath) {
        int maxLen = 0;
        for (var name: _schedule.keySet()) {
            if (_schedule.get(name).size() > maxLen)
                maxLen = _schedule.get(name).size();
        }

        for (var name: _schedule.keySet()) {
            for (int i = _schedule.get(name).size(); i < maxLen; i++) {
                _schedule.get(name).add("Свободен");
            }
        }

        JSONObject jsonOutput = new JSONObject();
        JSONArray jsonScheduleArray = new JSONArray();

        for (var name: _schedule.keySet()) {
            JSONObject obj = new JSONObject();

            obj.put("name", name);
            obj.put("schedule", _schedule.get(name));

            jsonScheduleArray.add(obj);
        }

        jsonOutput.put("timetable", jsonScheduleArray);

        FileWriter outputFile = null;

        try {
            outputFile = new FileWriter(filepath);
            outputFile.write(jsonOutput.toJSONString());
            outputFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("\n### SCHEDULE SAVED ###\n");
    }

    @Override
    protected void takeDown() {
        // output data to file
        outputSchedule("output.json");

        System.out.println(getAID().getLocalName() + " destroyed\n");
    }

}