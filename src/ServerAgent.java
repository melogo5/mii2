import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ServerAgent extends Agent {

    public ArrayList<ArrayList<Long>> _matrix;

    long dijkstra(int start, int end, int vNum) {
        int INF = Integer.MAX_VALUE / 2;
        boolean[] used = new boolean [vNum]; // массив пометок
        long[] dist = new long [vNum]; // массив расстояния. dist[v] = минимальное_расстояние(start, v)
        long[] previous = new long [vNum];

        for (int i=0; i<vNum; i++)
        {
            previous[i]=-1;
        }
        Arrays.fill(dist, INF); // устанаавливаем расстояние до всех вершин INF
        dist[start] = 0; // для начальной вершины положим 0
        for (;;) {
            int v = -1;
            for (int nv = 0; nv < vNum; nv++) // перебираем вершины
                if (!used[nv] && dist[nv] < INF && (v == -1 || dist[v] > dist[nv])) // выбираем самую близкую непомеченную вершину
                    v = nv;
            if (v == -1) break; // ближайшая вершина не найдена
            used[v] = true; // помечаем ее
            for (int nv = 0; nv < vNum; nv++)
                if (!used[nv] && _matrix.get(v).get(nv) < INF) // для всех непомеченных смежных
                {
                    if (dist[nv]>dist[v] + _matrix.get(v).get(nv))
                    {
                        dist[nv] = dist[v] + _matrix.get(v).get(nv);
                        previous[nv] = v;
                    }

                }
        }

        ArrayList<Integer> way = new ArrayList<Integer>();
        ArrayList<Integer> govnofix = new ArrayList<Integer>();
        if (dist[end]>0 && dist[end]<INF)
        {
            govnofix.add(end);
            int t = (int) previous[end];
            govnofix.add(t);
            while(t != start)
            {
                t = (int) previous[t];
                govnofix.add(t);
            }
        }
        else
            govnofix.add(-1);

        for (int i = govnofix.size() - 1; i >= 0; --i)
            way.add(govnofix.get(i));
        return getLength(_matrix, way);
    }

    public long getLength(ArrayList<ArrayList<Long>> info, ArrayList<Integer> route) {
        int sum = 0;
        for(int i = 0; i < route.size() - 1; i++) {
            int from = route.get(i);
            int to = route.get(i+1);
            sum += info.get(from).get(to);
        }
        return sum;
    }

    @Override
    protected void setup() {
        _matrix = new ArrayList<>();
        Object[] args = getArguments();

        String filePath = (String) args[0];

        var parser = new JSONParser();
        try (Reader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), StandardCharsets.UTF_8))) {
            var jsonInput = (JSONObject) parser.parse(reader);
            var n = (long) jsonInput.get("n");
            for (int i = 0; i < n; i++) {
                var row = (ArrayList<Long>) jsonInput.get("r" + i);
                _matrix.add(row);
            }
        } catch (IOException e) {
        } catch (ParseException e) {
            e.printStackTrace();
        }

        System.out.println("Server is ready!");
        System.out.println(_matrix);

        registerService();

        addBehaviour(new waitQuery());
    }

    private void registerService() {
        DFAgentDescription dfa = new DFAgentDescription();
        dfa.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("ServerAgent");
        sd.setName(getLocalName());
        dfa.addServices(sd);

        try {
            DFService.register(this, dfa);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public class waitQuery extends CyclicBehaviour {
        public void action() {
            ACLMessage msg = myAgent.receive();
            if (msg != null) {
                // request from Train
                if (msg.getPerformative() == ACLMessage.REQUEST) {
                    // unpacking info
                    var parser = new JSONParser();
                    JSONObject jsonFromTrain = null;

                    try {
                        jsonFromTrain = (JSONObject) parser.parse(msg.getContent());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    String cargoName = (String) jsonFromTrain.get("cargoName");
                    long from = (long) jsonFromTrain.get("from");
                    long to = (long) jsonFromTrain.get("to");

                    long distance = dijkstra((int) from, (int) to, _matrix.size());

                    ACLMessage messageToTrain = new ACLMessage(ACLMessage.INFORM);
                    // add requested data to the response
                    Map<String, Object> obj = new HashMap<>();
                    obj.put("cargoName", cargoName);
                    obj.put("distance", distance);

                    JSONObject jsonToTrain = new JSONObject();
                    jsonToTrain.putAll(obj);

                    messageToTrain.setContent(jsonToTrain.toJSONString());
                    messageToTrain.addReceiver(msg.getSender());

                    myAgent.send(messageToTrain);

                    System.out.println(distance + " Calculated distance for " + cargoName + "\nFrom: " + from + " To: " + to);
                }
            }
        }
    }

    @Override
    protected void takeDown() {
        System.out.println(getAID().getLocalName() + " destroyed\n");
    }
}
