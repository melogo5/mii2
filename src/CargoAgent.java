import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.wrapper.ControllerException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.HashMap;
import java.util.Map;

public class CargoAgent extends Agent {
    public long _typeID;
    public long _availableCount;
    public long _to;
    public long _from;

    public AID _mainAID;

    public Map<String, Map<String, Long>> _potentialTrains;

    @Override
    protected void setup() {
        Object args[] = getArguments();

        _typeID = (long) args[0];
        _to = (long) args[1];
        _from =  (long) args[2];
        _availableCount = (long) args[3];

        _mainAID = (AID) args[4];

        _potentialTrains = new HashMap<>();

        System.out.println(getLocalName() + "(" + _typeID + ") is ready!");

        registerService();

        addBehaviour(new searchTrain());
        addBehaviour(new chooseTrain());
    }

    private void registerService() {
        DFAgentDescription dfa = new DFAgentDescription();
        dfa.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("Cargo");
        sd.setName(getLocalName());
        dfa.addServices(sd);

        try {
            DFService.register(this, dfa);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public class chooseTrain extends CyclicBehaviour {
        @Override
        public void action() {
            ACLMessage msg = myAgent.receive();
            if (msg != null) {
                switch (msg.getPerformative()) {
                    case ACLMessage.PROPOSE:
                        var parser = new JSONParser();
                        JSONObject jsonObject = null;

                        try {
                            jsonObject = (JSONObject) parser.parse(msg.getContent());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        HashMap<String, Long> times = new HashMap<>();
                        times.put("duration", (long) jsonObject.get("duration"));
                        times.put("busy", (long) jsonObject.get("busy"));

                        _potentialTrains.put((String) jsonObject.get("name"), times);

                        if (_potentialTrains.size() == _availableCount) {
                            // choose best variant
                            String trainName = _potentialTrains.keySet().toArray()[0].toString();
                            long duration = _potentialTrains.get(trainName).get("duration");
                            long busy = _potentialTrains.get(trainName).get("busy");
                            long sumTime = duration + busy;
                            for (var name: _potentialTrains.keySet()) {
                                if (_potentialTrains.get(name).get("duration") + _potentialTrains.get(name).get("busy") < sumTime) {
                                    duration = _potentialTrains.get(name).get("duration");
                                    trainName = name;
                                    sumTime = duration + _potentialTrains.get(name).get("busy");
                                }
                            }

                            ACLMessage messageToTrain = new ACLMessage(ACLMessage.AGREE);

                            JSONObject jsonToTrain = new JSONObject();

                            busy = _potentialTrains.get(trainName).get("busy");
                            jsonToTrain.put("busy", busy);
                            jsonToTrain.put("duration", duration);

                            messageToTrain.setContent(jsonToTrain.toJSONString());
                            messageToTrain.addReceiver(getAID(trainName));

                            myAgent.send(messageToTrain);
                        }

                        break;

                    case ACLMessage.REFUSE:
                        ACLMessage messageToTrains = new ACLMessage(ACLMessage.REQUEST);
                        DFAgentDescription tmpTrains = new DFAgentDescription();
                        ServiceDescription sdTrains = new ServiceDescription();
                        sdTrains.setType("Train");
                        tmpTrains.addServices(sdTrains);

                        try {
                            DFAgentDescription[] res = null;

                            while (res == null || res != null && res.length < 1) {
                                res = DFService.search(myAgent, tmpTrains);
                            }

                            JSONObject jsonToTrain = new JSONObject();
                            jsonToTrain.put("typeID", _typeID);
                            jsonToTrain.put("to", _to);
                            jsonToTrain.put("from", _from);

                            messageToTrains.setContent(jsonToTrain.toJSONString());

                            for (var train: res) {
                                messageToTrains.addReceiver(train.getName());
                            }

                            myAgent.send(messageToTrains);
                        } catch (FIPAException e) {
                            e.printStackTrace();
                        }

                        break;

                    case ACLMessage.ACCEPT_PROPOSAL:
                        ACLMessage messageToMain = new ACLMessage(ACLMessage.INFORM);
                        messageToMain.addReceiver(_mainAID);
                        myAgent.send(messageToMain);

                        try {
                            getContainerController().getAgent(getLocalName()).kill();
                        } catch (ControllerException e) {
                            e.printStackTrace();
                        }

                        break;
                }
            }
        }
    }

    public class searchTrain extends OneShotBehaviour {
        @Override
        public void action() {
            ACLMessage messageToTrains = new ACLMessage(ACLMessage.REQUEST);
            DFAgentDescription tmpTrains = new DFAgentDescription();
            ServiceDescription sdTrains = new ServiceDescription();
            sdTrains.setType("Train");
            tmpTrains.addServices(sdTrains);

            try {
                DFAgentDescription[] res = null;

                while (res == null || res != null && res.length < 1) {
                    res = DFService.search(myAgent, tmpTrains);
                }

                JSONObject jsonToTrain = new JSONObject();
                jsonToTrain.put("typeID", _typeID);
                jsonToTrain.put("to", _to);
                jsonToTrain.put("from", _from);

                messageToTrains.setContent(jsonToTrain.toJSONString());

                for (var train: res) {
                    messageToTrains.addReceiver(train.getName());
                }

                myAgent.send(messageToTrains);
            } catch (FIPAException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void takeDown() {
        System.out.println(getAID().getLocalName() + " destroyed\n");
    }
}
